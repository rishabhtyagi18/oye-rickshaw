import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import "../home/home.css";
import XLSX from 'xlsx';

function Home(props) {

    const [inputs, setInputs] = useState({});
    const [text, setText] = useState('');
    const [show, setShow] =useState({});
    const [data, setData] =useState();

    function singleApproval(id) {
        approval([String(id)])
    }

    function approval(ids) {
        let result = []
        data.map( (obj) => {
            if(ids.includes(String(obj.id))) {
                obj.action = 'Approved';
                result.push(obj)
            }
        });
        console.log(result)
    }
    function bulkApproval() {
        let approval_ids = []
        Object.keys(inputs).map((id) => {
        if (inputs[id]){
            approval_ids.push(id)
        }
        });
        approval(approval_ids);
    }

    function showRemark(id) {
        setShow({...show, [id]: true})
    }
    function hideRemark(id) {
        setShow({...show, [id]: false})
    }

    function rejectOne(id) {
        let result = []
        data.map( (obj) => {
            if(id === obj.id) {
                obj.action = 'Rejected';

                obj.remark = text[id]
                if(obj.remark) {
                    result.push(obj)
                }else{
                    alert("Please enter remark")
                }
                
            }
        });
        if (result.length > 0) {
            console.log(result);
            hideRemark(id)
        }
    }
    function setRemark(e, id) {
        setText({...text, [id]: e.target.value})
    }

    const handleInputChange = (event) => {
        if (isNaN(event.target.id)) 
            return;
            setInputs({
                ...inputs,
                [event.target.id]: event.target.checked
            });
    }


    function Upload() {
        const fileUpload = (document.getElementById('fileUpload'));
        const regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            let fileName = fileUpload.files[0].name;
            if (typeof (FileReader) !== 'undefined') {
                const reader = new FileReader();
                if (reader.readAsBinaryString) {
                    reader.onload = (e) => {
                        processExcel(reader.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                }
            } else {
                console.log("This browser does not support HTML5.");
            }
        } else {
            console.log("Please upload a valid Excel file.");
        }
    }
    
    function processExcel(data) {
        const workbook = XLSX.read(data, {type: 'binary'});
        const firstSheet = workbook.SheetNames[0];
        const excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
        excelRows.map( (obj) => {

            obj['id'] = obj['earning_id'];
            delete obj['earning_id'];
        });
        setData(excelRows)
    }

  return (
    <>
     <div className="App">
        {!data &&
            <input class="chooseFile" type="file" id="fileUpload" onChange={Upload} />
        }
       {data &&
        <div>
            <div className="align-buttons">
            <button 
                className="main-button" 
                onClick={() => {
                    bulkApproval()
                }}>Approve
            </button>
            </div>
        
            <table>
                <thead>
                    <tr className="light-grey"> 
                        <td></td>
                        <td className="heading">Id</td>
                        <td className="heading">Mobile</td>
                        <td className="heading">Earning</td>
                        <td className="heading">Action</td>
                    </tr>
                    {data.map((item) => 
                    <>     
                        <tr>
                            <div className="checkbox">
                            <input 
                                type="checkbox"
                                id={item.id} 
                                onChange={handleInputChange}
                            />
                            </div>
                            <td>{item.id}</td>
                            <td>{item.mobile}</td>
                            <td>₹ {item.earning}</td>
                            <td>
                                {!show[item.id] && 
                                <>
                                    <button 
                                    className="single-button"
                                    onClick={() => {
                                        singleApproval(item.id)
                                    }}
                                    >Approve</button>
                                    &nbsp;
                                    |
                                    &nbsp;
                                    <button 
                                        className="single-button" 
                                        name="reject"
                                        onClick={() => {
                                            showRemark(item.id)
                                        }}
                                    >
                                        Reject
                                    </button>
                                </>
                                }
                                
                                
                                {show[item.id] &&
                                <>
                                    <textarea 
                                        placeholder="Remarks.."
                                        className="textArea"
                                        onChange={e => {
                                            setRemark(e, item.id)
                                        }}       
                                        >       
                                    </textarea>
                                    <button className="okButton"
                                        onClick={() => {
                                            rejectOne(item.id)
                                    }}>
                                    Ok
                                    </button>
                                    <button 
                                        className="cancelButton"
                                        onClick={() => {
                                            hideRemark(item.id)
                                        }}>
                                        Cancel
                                    </button>
                                    </>
                                }
                            </td>
                        </tr>
                    </>
                    )}
                </thead>
            </table>
        </div>
        }
    </div>
    </>
  );
}

export default withRouter(Home);
